<?php
  class Cliente extends CI_model{
    public function insertar($data){
      return $this->db->insert('cliente',$data);
    }

    public function obtenerTodos(){
      $query=$this->db->get('cliente');
      if ($query->num_rows()>0) {
        return $query;
      }else {
        return false;
      }
    }
    public function obtenerPorId($id){
      $this->db->where('id_cli',$id);
      $query=$this->db->get('cliente');
      if ($query->num_rows()>0) {
        return $query->row();
      }else {
        return false;
      }
    }

    public function eliminar($id_cli){
      $this->db->where('id_cli',$id_cli);
      return $this->db->delete('cliente');
    }

    public function actualizar($id,$data){
      $this->db->where('id_cli',$id);
      return $this->db->update('cliente',$data);
    }

  }


 ?>
