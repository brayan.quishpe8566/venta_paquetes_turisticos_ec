<div class="col-md-12 text-right">
<?php if ($listadoClientes): ?>
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">CODIGO</th>
        <th class="text-center">CÉDULA</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">EDAD</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoClientes->result() as $clienteTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $clienteTemporal->id_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->cedula_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->nombre_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->apellido_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->edad_cli ?></td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $clienteTemporal->id_cli; ?>" data-toggle="modal" data-target="#myModal2">

              <i class="glyphicon glyphicon-pencil" title="editar" ></i>
            </a>
            <a href="<?php echo site_url(); ?>/clientes/eliminarCliente/<?php echo $clienteTemporal->id_cli; ?>" onclick="confirmation(event)">
              <i class="glyphicon glyphicon-trash" title="editar"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
  No se encuentra clientes registrados
<?php endif; ?>
</div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">

        <h4 class="modal-title text-center text-dark ">
          <b>Editar Cliente</b>
        </h4>
      </div>
      <br><br>
      <div class="modal-body" id="contenedor_formulario_cliente">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    function cargarClientes(){
      $("#contenedor_formulario_cliente").load('<?php echo site_url('clientes/editar'); ?>');
    }
    cargarClientes();
</script>
