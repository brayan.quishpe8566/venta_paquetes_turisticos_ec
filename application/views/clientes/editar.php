<form enctype="multipart/form-data" class="" id="frm_editar_jugador" action="<?php echo site_url(); ?>/clientes/actualizarCliente" method="post">
  <div class="row">
    <div class="col-md-12 text-center text-dark">
      <br>
      <label for="" class="text-dark">Editar Cliente</label>
      <legend></legend>
    </div>

  </div>
  <div class="row">
    <div class="col-md-5 text-center">
      <br><br><br>
      <label for="">Cédula:</label><br><br>
      <label for="">Nombres:</label><br><br>
      <label for="">Apellidos:</label><br><br><br>
      <label for="">Edad:</label><br><br><br>
    </div>
    <div class="col-md-7 text-left">
      <div class="form-group">
        <br><br><br>
        <input type="hidden" name="id_cli" id="id_cli" value="<?php echo $clienteEditar->id_cli;?>">
        <!-- <input style="color:black !important;" class="form-control" required placeholder="Nombre del equipo" type="text" name="nombre_equi" id="nombre_equi" value="<?php echo $formularioEquipo->nombre_equi; ?>"> -->
      </div>

			<input style="color:black !important;" class="form-control" required placeholder="Cédula del cliente"type="number" name="cedula_cli" id="cedula_cli" value="<?php echo $clienteEditar->cedula_cli; ?>">
			<input style="color:black !important;" class="form-control" required placeholder="Nombre del cliente" type="text" name="nombre_cli" id="nombre_cli" value="<?php echo $clienteEditar->nombre_cli; ?>"><br>
			<input style="color:black !important;" class="form-control" required placeholder="Apellido del cliente" type="text" name="apellido_cli" id="apellido_cli" value="<?php echo $clienteEditar->apellido_cli; ?>"><br>
      <input style="color:black !important;" class="form-control" required placeholder="Edad del cliente" type="text" name="edad_cli" id="edad_cli" value="<?php echo $clienteEditar->edad_cli; ?>"><br>
    </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col-md-6 text-center">
          <button type="submit" class="btn btn-success" name="button" >ACTUALIZAR</button>

      </div>
      <div class="col-md-6">
        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
      </div>

    </div>
  </div>
</form>


<script type="text/javascript">
  $("#frm_editar_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10,
        remote:{
          url:"<?php echo site_url('clientes/validarCedulaExistente') ?>",
          data:{
            "cedula_cli":function(){
              return $("#cedula_cli").val();
            }
          },
          type:"post"
        }
      },
      nombre_cli:{
        required:true
      },
      apellido_cli:{
        required:true
      },
      edad_cli:{
        required:true,
        digits:true
      }
    },
    messages:{
      cedula_cli:{
        required:"Por favor ingrese la cedula",
        digits:"Por favor ingrese solo numeros",
        maxlength:"Por favor ingrese 10 digitos",
        minlength:"Por favor ingrese 10 digitos",
        remote:"Este numero de cedula ya esta en uso"
      },
      nombre_cli:{
        required:"Por favor ingrese los nombres"
      },
      apellido_cli:{
        required:"Por favor ingrese los apellidos"
      },
      edad_cli:{
        required:"Por favor ingrese la edad",
        igits:"Por favor ingrese solo numeros"
      }
    },
    submitHandler:function(form){
        var url=$(form).prop("action");//capturando url (controlador/funcion)
        //generando peticion asincrona
         var formData=new FormData($("#frm_editar_cliente")[0]);
        $.ajax({
             url:url,//action del formulario
             type:'post',//definiendo el tipo de envio de datos post/get
             data:$(form).serialize(), //enviando los datos ingresados en el formulario
             success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                alert('Cliente actualizado exitosamente');
                cargarClientes();//llamndo la funcion para actualizar el listado clientes
                $(form)[0].reset();
                cerrarModal();
                Swal.fire({
                  background:'#0C062E',
                  color:'#FFF',
                  title: 'Confirmacion',
                  text: "Cliente Guardado Exitosamente",
                  icon: 'success',
                });
             },
             error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                alert('Error al actalizar, intente nuevamente');
             }
        });
      }
  });

</script>
