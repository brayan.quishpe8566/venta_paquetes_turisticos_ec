<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Adventure - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/animate.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/jquery.timepicker.css">


    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">

		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- Imprtando Datatables. -->
	<!-- <link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"> -->
	<!-- <script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script> -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>

	<?php if ($this->session->flashdata('confirmacion')): ?>
		<script type="text/javascript">
			$(document).ready(function(){
				Swal.fire(
					'CONFIRMACIÓN',
					'<?php echo $this->session->flashdata('confirmacion') ?>',
					'success'
				);
			});
		</script>
	<?php endif; ?>
	<?php if ($this->session->flashdata('error')): ?>
		<script type="text/javascript">
			$(document).ready(function(){
				Swal.fire(
					'ERROR',
					'<?php echo $this->session->flashdata('error') ?>',
					'error'
				);
			});
		</script>
	<?php endif; ?>
	<!--importando file input-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/fileinput.min.js" integrity="sha512-CCLv901EuJXf3k0OrE5qix8s2HaCDpjeBERR2wVHUwzEIc7jfiK9wqJFssyMOc1lJ/KvYKsDenzxbDTAQ4nh1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/css/fileinput.min.css" integrity="sha512-qPjB0hQKYTx1Za9Xip5h0PXcxaR1cRbHuZHo9z+gb5IgM6ZOTtIH4QLITCxcCp/8RMXtw2Z85MIZLv6LfGTLiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.2.7/js/locales/es.min.js" integrity="sha512-Mu+FX6b1AzpD49KmO13T8uLDpHK8M1vgZ75094jEP4KV0j59xJFfRz/SP0QIr/9dmHc4yIUGEFcnTU3uhHJaJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  </head>
  <body>

	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">Adventure</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item active"><a href="<?php echo base_url(); ?>" class="nav-link">Inicio</a></li>
	          <li class="nav-item"><a href="<?php echo site_url(); ?>/clientes/index" class="nav-link">Clientes</a></li>
	          <li class="nav-item"><a href="places.html" class="nav-link">Places</a></li>
	          <li class="nav-item"><a href="hotel.html" class="nav-link">Hotels</a></li>
	          <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
	        </ul>
	      </div>
	    </div>
			<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript" src="https://unpkg.com/chart.js-plugin-labels-dv/dist/chartjs-plugin-labels.min.js"></script>
	  </nav>
    <!-- END nav -->
