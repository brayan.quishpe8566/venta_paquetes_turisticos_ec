<!-- package section -->

<section class="package_section" id="package">
  <div class="container">
    <div class="heading_container">
      <h2>
        LISTADO CLIENTES
      </h2>
    </div>
  </div>
  <div class="container">
      <div class="img-box">
        <img src="<?php echo base_url(); ?>/assets/images/package-img.png" alt="">
      </div>
      <div class="detail-container">
  <div class="row">
    <div class="col-md-5">
      <center>
        <h4 style="font-wieght:bold;">NUEVO CLIENTE</h4>
      </center>
  </div>
  <div class="col-md-7">
    <button type="button" name="button" class="site-btn" onclick="cargarEquipos();">
      Actualizar Datos
    </button>
    <br>
    <div id="contenedor_listado_clientes">
    </div>
  </div>

      </div>
      <br><br>
      <center>
      <div class="row">
      <div class="col-md-12 text-left">
      <!-- <center> -->
      <button type="button" class="site-btn" data-toggle="modal" data-target="#myModal">Agregar Cliente</button>

      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">
                <b>
                NUEVO CLIENTE
                </b>
              </h4>
            </div>
            <div class="modal-body">
              <p>Formulario de nuevo Cliente.</p>
              <section>
                <div class="container">
                  <h2>NUEVO CLIENTE</h2>
                </div>
              </section>
              <!-- Page top section end -->
              <!-- Character section 1 -->
              <section class="characters-section character-one">
                <div class="container">
                  <div class="row">
                    <div class="col-md-3">
                    </div>
                  <div class="col-md-6">
                    <form class="" enctype="multipart/form-data" action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente">
                      <table class="table">
                        <tr>
                        <td><label for="">Cédula</label></td>
                        <td><input type="number" name="cedula_cli" id="cedula_cli" class="form-control"
                        value="" placeholder="Ingrese su cedula: " required autocomplete="off"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>Ej. 1718192023</td>
                      </tr>
                      <tr>
                        <td><label for="">Nombre: </label></td>
                        <td><input type="text" name="nombre_cli" id="nombre_cli" class="form-control"
                        value="" placeholder="Ingrese el nombre del Cliente" required autocomplete="off"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>Ej. Juan Carlos</td>
                      </tr>
                      <tr>
                        <td><label for="">Apellido: </label></td>
                        <td><input type="text" name="apellido_cli" id="apellido_cli" class="form-control"
                        value="" placeholder="Ingrese el apellido del Cliente" required autocomplete="off"></td>
                    </tr>
                      <tr>
                        <td></td>
                        <td>Ej. Marinez Arias</td>
                      </tr>
                      <tr>
                        <td><label for="">Edad: </label></td>
                        <td><input type="number" name="edad_cli" id="edad_cli" class="form-control"
                        value="" placeholder="Ingrese la edad del Cliente" required autocomplete="off"></td>
                    </tr>
                      <tr>
                        <td></td>
                        <td>Ej. 20</td>
                      </tr>
                    </table><center>
                        <div class="row">
                          <div class="col-md-8">
                            <button type="submit" name="button" class="site-btn" >Guardar Cliente</button>
                            <button type="button" name="button" class="site-btn sb-color" onclick="cerrarModal();">Cancelar Acción</button>
                          </div>
                          <div class="col-md-4" text-right>
                             <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                          </div>
                        </div>
                </form>
              </div>
              <div class="col-md-3">

              </div>
              </div>
              </div>
            </section>
              <!-- Character section 1 end -->
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</center>
    <!-- </center> -->
  </div>
    </div>
    </div>
</section>

<!-- Character section 1 end -->
<script>
function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>

<script type="text/javascript">
  $("#frm_nuevo_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10,
        remote:{
          url:"<?php echo site_url('clientes/validarCedulaExistente') ?>",
          data:{
            "cedula_cli":function(){
              return $("#cedula_cli").val();
            }
          },
          type:"post"
        }
      },
      nombre_cli:{
        required:true
      },
      apellido_cli:{
        required:true
      },
      edad_cli:{
        required:true,
        digits:true
      }
    },
    messages:{
      cedula_cli:{
        required:"Por favor ingrese la cedula",
        digits:"Por favor ingrese solo numeros",
        maxlength:"Por favor ingrese 10 digitos",
        minlength:"Por favor ingrese 10 digitos",
        remote:"Este numero de cedula ya esta en uso"
      },
      nombre_cli:{
        required:"Por favor ingrese los nombres"
      },
      apellido_cli:{
        required:"Por favor ingrese los apellidos"
      },
      edad_cli:{
        required:"Por favor ingrese la edad",
        igits:"Por favor ingrese solo numeros"
      }
    },
    submitHandler:function(form){
        var url=$(form).prop("action");//capturando url (controlador/funcion)
        //generando peticion asincrona
        $.ajax({
             url:url,//action del formulario
             type:'post',//definiendo el tipo de envio de datos post/get
             data:$(form).serialize(), //enviando los datos ingresados en el formulario
             success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
                alert('Cliente guardado exitosamente');
                cargarClientes();//llamndo la funcion para actualizar el listado clientes
                $(form)[0].reset();
                cerrarModal();
                Swal.fire({
                  background:'#0C062E',
                  color:'#FFF',
                  title: 'Confirmacion',
                  text: "Cliente Guardado Exitosamente",
                  icon: 'success',
                });
             },
             error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
                alert('Error al insertar, intente nuevamente');
             }
        });
      }
  });

</script>
<script type="text/javascript">
function cargarClientes(){
  $("#contenedor_listado_clientes").load('<?php echo site_url("clientes/tablaClientes"); ?>');
}
cargarClientes();
</script>
<!-- Trigger the modal with a button -->
<script type="text/javascript">
//Funcion para cerrar la ventana Modal
  function cerrarModal(){
    $("#myModal").modal("hide");
  }
</script>

<!-- end package section -->
