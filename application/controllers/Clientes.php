<?php
  class Clientes extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('cliente');
    }

    public function index(){
      // $data["listadoEquipos"]=$this->equipo->obtenerTodos();
      $this->load->view('encabezado');
      $this->load->view('clientes/index');
      $this->load->view('pie');
    }

    public function editar(){
      $this->load->view('encabezado');
      $this->load->view('clientes/editar');
      $this->load->view('pie');
    }

    public function tablaClientes(){
      $data["listadoClientes"]=$this->cliente->obtenerTodos();
      $this->load->view('clientes/tablaClientes',$data);
    }
    public function guardarCliente(){
      $cedula=$this->input->post('cedula_cli');
      $nombre=$this->input->post('nombre_cli');
      $apellido=$this->input->post('apellido_cli');
      $edad=$this->input->post('edad_cli');

      $datosNuevoCliente=array(
        "cedula_cli"=>$cedula,
        "nombre_cli"=>$nombre,
        "apellido_cli"=>$apellido,
        "edad_cli"=>$edad
      );
      if ($this->cliente->insertar($datosNuevoCliente)) {
        $this->session->set_flashdata('confirmacion','Cliente registrado');
        redirect('clientes/index');
      } else {
        echo "Cliente no guardado";
      }
    }

    public function eliminarCliente($id){
      if($this->cliente->eliminar($id)){
        $this->session->set_flashdata('confirmacion','Cliente eliminado');
        redirect('clientes/index');
      }else {
        echo "error al eliminar";
      }
    }
  }
?>
